array = ["abcde","abdf","adeab","abdgeee","bdefa","abc","ab","a","bacdef"]

sortarray = array.sort_by(&:length)

print "Original array: " 
print array 

print "\n Sorted array of strings by length: " 
print sortarray
puts "\n"