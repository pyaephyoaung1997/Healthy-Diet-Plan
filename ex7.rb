array = [10,20,30,40,10,10,20]
print "Original array: \n"
print array

freq_num = array.inject(Hash.new(0)){ |h,v| h[v] +=1; h }
print "\n Frequency of number: \n"
print freq_num
print "\n"